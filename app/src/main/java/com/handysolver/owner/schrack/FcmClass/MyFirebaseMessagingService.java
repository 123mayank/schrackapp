package com.handysolver.owner.schrack.FcmClass;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.handysolver.owner.schrack.GlobalConstants;
import com.handysolver.owner.schrack.MainActivity;
import com.handysolver.owner.schrack.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.StringReader;
import java.util.Random;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String title2 = "";
        if (remoteMessage.getNotification()!= null){
            title2 = remoteMessage.getNotification().getTitle();
        }else{
            String responseString = remoteMessage.getData().toString();
            Log.e("notification","recieved "+responseString);
            JSONObject obj=GlobalConstants.returnObject(responseString);
            if(obj!=null){
                try{
                    String responseData = obj.getString("response");
                    String title = obj.getString("title");

                    JSONObject responseObj=GlobalConstants.returnObject(responseData);
                    String message=responseObj.getString("SP-PR-NAME");
                    // fetch all data
                    sendNotification(title,message, responseObj);
                    Log.d(TAG, "onMessageReceived: "+responseObj.toString());
                }catch(JSONException ex){

                }

                Log.d(TAG, "onMessageReceived: "+obj);
            }
        }


        //sendNotification(title, message);
    }
    private void sendNotification(String title, String message, JSONObject response) {
        //Log.d(TAG, "sendNotification: "+messageObject.getString("SP-PR-NAME"));
        Intent intent = new Intent(this, MainActivity.class);
        Bundle extras = new Bundle();
        extras.putString(GlobalConstants.RESPONSE_OBJECT, response.toString());
        intent.putExtras(extras);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),
                        R.mipmap.ic_launcher))
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(getRequestCode(), notificationBuilder.build());
    }

    private static int getRequestCode() {
        Random rnd = new Random();
        return 100 + rnd.nextInt(900000);
    }
}




