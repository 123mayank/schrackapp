package com.handysolver.owner.schrack;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class MainActivity extends AppCompatActivity {
    TextView data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        data=(TextView) findViewById(R.id.data);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if(extras!=null){
            String responseString=extras.getString(GlobalConstants.RESPONSE_OBJECT);
            Log.d("response", "onCreate: "+responseString);
            JSONObject obj=GlobalConstants.returnObject(responseString);
            String textData="";
            if(obj!=null){
                try{
                    Iterator<?> keys = obj.keys();

                    while( keys.hasNext() ) {
                        String key = (String)keys.next();
                        Log.d("key", "onCreate: "+key);
                        textData+="<strong>"+key+"</strong><span>: "+obj.get(key)+"</span><br>";
                    }

                }catch(JSONException ex){
                }

            }
            data.setText(Html.fromHtml(textData));
        }

    }
}
