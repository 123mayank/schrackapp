package com.handysolver.owner.schrack;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by root on 5/6/16.
 */
public class GlobalConstants {
    public static final String RESPONSE_OBJECT = "RESPONSE_OBJECT";
    public static JSONObject returnObject(String json){
        JSONObject obj=null;
        try {

            obj = new JSONObject(json);

        } catch (Throwable t) {
            Log.e("My App", "Could not parse malformed JSON: \"" + json + "\"");
        }
        return obj;
    }
}

